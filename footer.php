<?php
/**
 * @package Serene
 * @since Serene 1.0
 */
?>
</div> <!-- end footer -->

	<!-- aside widget aria -->
	<?php if ( is_active_sidebar( 'aside-col' ) ) : ?>    
    	<div class="aside-widget-column">  
            <?php dynamic_sidebar( 'aside-col' ); ?>
	    <div class="social-media-container">
		<h4>Social Media</h4>
		<ul>
	        	<li><a href="https://www.linkedin.com/company/adtailor" class="linkedin" target="_blank">linkedin</a></li>
	        	<li><a href="https://www.facebook.com/adtailor/?fref=ts" class="facebook" target="_blank">facebook</a></li>
	        	<li><a href="https://twitter.com/adtailor " class="twitter" target="_blank">twitter</a></li>
		</ul>
            </div>		
	</div><!--- /second-footer-column -->
	<?php endif; ?>
	<!-- end aside widget aria -->
		<footer id="main-footer">
			<?php get_sidebar( 'footer' ); ?>
		<!--	<p id="footer-info">
				<a href="http://wordpress.org/" rel="generator">Proudly powered by WordPress</a>
				<?php printf( __( 'Theme: %1$s by %2$s.', 'Serene' ), 'Serene', '<a href="http://www.elegantthemes.com/" rel="designer">Elegant Themes</a>' ); ?>
			</p>-->
		</footer>  <!-- #main-footer -->
         	 <div class="close-signup-newsletter">
			<i></i>
			<i></i>
		</div>	

	</div> <!-- #container -->




	<?php wp_footer(); ?>

  	<!-- main nav -->

<script src="https://code.jquery.com/jquery-3.1.0.min.js" integrity="sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/1.11.8/semantic.min.js"></script>

<?php if(is_single()) { ?>
<script>
  $(window).ready(function() {
    setTimeout(function() {
    $('.page-loader').fadeOut().css('z-index', '-1')
    }, 300)
  });
</script>

<div class="ui segment page-loader" style="margin:0;padding:0;height:100%;position:absolute;left:0;right:0;top:0;bottom:0;"><div class="ui active inverted dimmer"><div class="ui text loader"></div></div><p></p></div>
<?php } ?>
</body>
</html>
