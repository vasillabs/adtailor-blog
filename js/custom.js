(function($){
	"use strict";

	$(document).ready(function(){

		if( localStorage.getItem('beforeunload') === null ) { 
			 localStorage.setItem('beforeunload',1);
		}

		if( localStorage.getItem('scrollForm')== null ) {
			localStorage.setItem('scrollForm',1);	
		}
		// mouseout activate pop up signup 
		$(document).one("mouseleave" , function () {
    		
			if(localStorage.getItem('beforeunload') == "1") {
				if ($('body').hasClass('single') /*|| $('body').hasClass('home')*/ ) {
					setTimeout(function() {
					$('.aside-widget-column,#email-subscribers-3').addClass('beforeunload');
					$("#email-subscribers-3").append("<div class='close-signup-newsletter'><i></i><i></i></div>").show();
					$(".close-signup-newsletter").on('click', function() {
						$(".aside-widget-column,#email-subscribers-3").removeClass('beforeunload');
					});
				
					localStorage.removeItem('beforeuload');	
					localStorage.setItem('beforeunload',0);
					}, 250);
				}

				if($('body').hasClass(('home'))) {
				}	
			} 

		});

		if($('body').hasClass('home')){
		   $(window).scroll( function() {
			if($(window).scrollTop() > 1500) {
				if(localStorage.getItem('scrollForm') == "1" && localStorage.getItem('scrollForm') !== "0" ) {
					$('.aside-widget-column,#email-subscribers-3').addClass('beforeunload');
					$("#email-subscribers-3").append("<div class='close-signup-newsletter'><i></i><i></i></div>").show();
					$(".close-signup-newsletter").on('click', function() {
						$(".aside-widget-column,#email-subscribers-3").removeClass('beforeunload');
					});
					localStorage.setItem("scrollForm","0");
				}
			}			
		   });
		}

		// insert MOUSEFLOW CONTROL
			$("body").prepend("<script>window.smartlook||(function(d){var o=smartlook=function(){o.api.push(arguments)},s=d.getElementsByTagName('script')[0];var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';c.charset='utf-8';c.src='//rec.getsmartlook.com/bundle.js';s.parentNode.insertBefore(c,s);})(document);smartlook('init','47685972971c5574d140c7a42f6740c5a8f00882');</script>");
		
		 // add hotjar heatmap
     $("body").append("<script>(function(h,o,t,j,a,r){h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};h._hjSettings={hjid:361091,hjsv:5};a=o.getElementsByTagName('head')[0];r=o.createElement('script');r.async=1;r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;a.appendChild(r);})(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');</script>");

	
		// edit 11.07 2016
			$("body").append("<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga'); ga('create', 'UA-80444514-1', 'auto'); ga('send', 'pageview');</script>");
				
		// section clicks
		
			$(".et-main-image-link").on("click", function() {
				var postUrl = $(this).attr("href");
				var postHeading = $(this).attr("href").split("/")[7].replace(/-/g, ' ');
				ga('send','event', '' + postHeading + '','Click','' + postUrl + '');
			});	
		

			
			$(".es_textbox_button").on("click" , function() {
				ga('send','event','Blog Subscribe','Click','Blog subscription button');
			});	
		//  edit 16.03 2016

			$(".close-signup-newsletter").on("click", function() {
				$(".aside-widget-column").slideUp();	
				$(".close-signup-newsletter, .close-signup-newsletter i").hide();
				localStorage.setItem("signup", "closed");
			});


		if(localStorage.getItem("signup") == "closed") {
			$(".aside-widget-column, .close-signup-newsletter i, .close-signup-newsletter").hide();
		}	
					
		function hideNav() {
			jQuery('.main-nav').fadeOut(500);

			jQuery('body').css({
				"height" : "auto",
				"overflow-y" : "visible"
			});

			jQuery("#fp-nav.right").show();
			jQuery(".header-content-top h1 a").removeClass("act fadeInDown");
		}

		function showNav() {
				jQuery('.main-nav').fadeIn(500);
				jQuery('body').css({
					"height" : jQuery(window).height(),
					"overflow-y" : "hidden"
				});

				jQuery("#fp-nav.right").hide();

				jQuery(".header-content-top h1 a").addClass("act fadeInDown");
		}

		jQuery('.toggle-mainmenu').on('click', function() {

				if(jQuery('.main-nav').is(":visible") == true) {
					jQuery(this).removeClass("act");
					hideNav();
				}

				else {
					jQuery(this).addClass("act");
					showNav();
				}		
		});
		
		
		var $main_menu    = $( '#main-header .nav' ),
			$format_video = $( '.format-video' ),
			$featured     = $( '.et-gallery-slider' ),
			$comment_form = $( '#commentform' ),
			$top_menu     = $( '#top-menu' );

		$main_menu.superfish({
			delay:       300,                            // one second delay on mouseout
			animation:   { height : 'show' },  // fade-in and slide-down animation
			speed:       'fast',                          // faster animation speed
			autoArrows:  true,                           // disable generation of arrow mark-up
			dropShadows: false                            // disable drop shadows
		});

		$format_video.find( '.main-image' ).fitVids();

		$format_video.find( '.et-main-image-link' ).click( function() {
			var $this_el = $(this),
				$video_container = $this_el.siblings( '.et-video-container' );

			if ( $video_container.length ) {
				$this_el.animate( { 'opacity' : 0 }, 500, function() {
					$(this).css( 'display', 'none' );
					$video_container.show();
				} );

				return false;
			}
		} );

		if ( $featured.length ){
			$featured.flexslider( {
				slideshow  : false,
				selector   : '.et-gallery-slides > li',
				controlNav : false
			} );
		}

		$comment_form.find('input:text, input#email, input#url, textarea').each(function(index,domEle){
			var $et_current_input = jQuery(domEle),
				$et_comment_label = $et_current_input.siblings('label'),
				et_comment_label_value = $et_current_input.siblings('label').text();
			if ( $et_comment_label.length ) {
				$et_comment_label.hide();
				if ( $et_current_input.siblings('span.required') ) {
					et_comment_label_value += $et_current_input.siblings('span.required').text();
					$et_current_input.siblings('span.required').hide();
				}
				$et_current_input.val(et_comment_label_value);
			}
		}).bind('focus',function(){
			var et_label_text = jQuery(this).siblings('label').text();
			if ( jQuery(this).siblings('span.required').length ) et_label_text += jQuery(this).siblings('span.required').text();
			if (jQuery(this).val() === et_label_text) jQuery(this).val("");
		}).bind('blur',function(){
			var et_label_text = jQuery(this).siblings('label').text();
			if ( jQuery(this).siblings('span.required').length ) et_label_text += jQuery(this).siblings('span.required').text();
			if (jQuery(this).val() === "") jQuery(this).val( et_label_text );
		});

		// remove placeholder text before form submission
		$comment_form.submit(function(){
			$comment_form.find('input:text, input#email, input#url, textarea').each(function(index,domEle){
				var $et_current_input = jQuery(domEle),
					$et_comment_label = $et_current_input.siblings('label'),
					et_comment_label_value = $et_current_input.siblings('label').text();

				if ( $et_comment_label.length && $et_comment_label.is(':hidden') ) {
					if ( $et_comment_label.text() == $et_current_input.val() )
						$et_current_input.val( '' );
				}
			});
		});

		et_duplicate_menu( $('#main-header ul.nav'), $('#main-header .mobile_nav'), 'mobile_menu', 'et_mobile_menu' );

		function et_duplicate_menu( menu, append_to, menu_id, menu_class ){
			var $cloned_nav;

			menu.clone().attr('id',menu_id).removeClass().attr('class',menu_class).appendTo( append_to );
			$cloned_nav = append_to.find('> ul');
			$cloned_nav.find('.menu_slide').remove();
			$cloned_nav.find('li:first').addClass('et_first_mobile_item');

			append_to.click( function(){
				if ( $(this).hasClass('closed') ){
					$(this).removeClass( 'closed' ).addClass( 'opened' );
					$cloned_nav.slideDown( 500 );
				} else {
					$(this).removeClass( 'opened' ).addClass( 'closed' );
					$cloned_nav.slideUp( 500 );
				}
				return false;
			} );

			append_to.find('a').click( function(event){
				event.stopPropagation();
			} );
		}

		$top_menu.append( '<span id="et_active_menu_item"></span>' );

		var $current_item_border = $( '#et_active_menu_item' ),
			$current_menu_item   = $top_menu.find( '> ul > .current-menu-item' ),
			current_item_width,
			current_item_position;

		function et_highlight_current_menu_item( $element, animation ) {
			if ( $element.length ) {
				current_item_width    = $element.width();
				current_item_position = $element.position().left;

				if ( ! animation ) {
					$current_item_border.css( { 'left' : current_item_position, 'width' : current_item_width } );
				} else {
					$current_item_border.animate( { 'left' : current_item_position }, 250, function() {
						$(this).animate( { 'width' : current_item_width }, 250 );
					} );
				}
			}
		}

		et_highlight_current_menu_item( $current_menu_item, false );

		$top_menu.find( '> ul > li' ).hover( function() {
			et_highlight_current_menu_item( $(this), true );
		}, function() {
			/*et_highlight_current_menu_item( $current_menu_item, true );*/
		} );
	} );
})(jQuery)
